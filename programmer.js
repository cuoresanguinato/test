$(document).ready(function(){

	$('#load').on('click', function(){

		let values_count = Math.floor(Math.random() * 101);
		let url = 'https://randomuser.me/api?results=' + values_count;

		$.ajax({
		    url: url,
		    type: 'GET',
		    dataType : "json",
		    success: function (results) {
				renderUserInfo(results.results);
				renderStatistics(results.results);
		    }               
		});

	});

});


function renderUserInfo(results)
{
	let total = results.lenght;

	const user_block = `
		<table class="users_section" border=1 cellpadding="5px">
		<tr>
			<th>Thumbnail</th>
			<th>Name, Gender</th>
			<th>Phone</th>
			<th>Email</th>
			<th>Address</th>
			<th>Date of Birth, Date registration</th>
		</tr>
		${results.map(user => `
			<tr class="user">
				<td>
					<img src="${user.picture.thumbnail}">
				</td>
				<td>
					${user.name.first + ' ' + user.name.last + ', ' + user.gender}
				</td>
				<td>
					${user.cell.replace(/\D/g,'').replace(/(\d{3})(\d{3})(\d{2})(\d{2})/,"+($1) $2-$3-$4")}
				</td>
				<td>
					${user.email}
				</td>
				<td>
					${user.location.city + ', ' + user.location.state + ', ' + user.location.street.name + ', ' + user.location.street.number}
				</td>
				<td>
					${new Date(user.dob.date).toDateString()}
				</td>
			</tr>
			`).join('')}
		</table>
	`;

	document.getElementById('user_results').innerHTML = user_block;
}

function renderStatistics(results)
{
	let male = 0;
	let female = 0;
	let nationals = new Object;
	let nat_render = '';

	results.map(user => nationals.hasOwnProperty(user.nat) ? nationals[user.nat] += 1 : nationals[user.nat] = 1); 
	results.map(user => user.gender == 'male' ? ++male : ++female);

	$.each(nationals, function(n,c){nat_render = nat_render + n + ': ' + c + '\n'});

	const statistic = `
	<table border=1 cellpadding="10px" style="margin-top:15px;">
		<tr>
			<th>Count</th>
			<th>Male</th>
			<th>Famale</th>
			<th>Count By Nationality</th>
		</tr>
		<tr>
			<td>${results.length}</td>
			<td>${male}</td>
			<td>${female}</td>
			<td>${nat_render}</td>
		</tr>
	</table>`;

	document.getElementById('user_stat').innerHTML = statistic;
}